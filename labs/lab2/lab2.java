
//
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Gregory M. Kapfhammer
// Ziyang Zhang
// CMPSC 111 Spring 2017
// Lab 2
// Jan 26 2017
//
// Purpose: to compute and print the number of the interest of saving ten thousand dollars for a year and two months.
//
import java.util.Date; // needed for printing today's date

public class lab2
{
	//main method: program execution begins here
	public static void main(String[] args)
	{
	  // Label output with name and date:
	  System.out.println("Ziyang\nlab 2\n" + new Date() + "\n");
	  // Variable:
	  double int2m = 1.05; // interest rate of 2 month deposit
	  double int1y = 1.03; // interest rate of 1 year deposit
	  int money = 10000; // the money to deposit
	  double money1; // the money after 1 year
	  double money2; // the money after 1 year and 2 months
	  // Compute values:
	  money1 = money * int1y; // compute the money after 1 year
	  money2 = money1 * int2m; // compute the money after 1 year and 2 month
	  System.out.println("The money deposited at first: " + money);
	  System.out.println("The money could be withdrawed");
	  System.out.println("after one year: " + money1);
	  System.out.println("The money could be withdrawed");
	  System.out.println("after one year and two months: " + money2);
	}
}


