//=================================================
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Ziyang Zhang
// CMPSC 111 Spring 2017
// Lab 4
// Feb 9 2017
//=================================================
import java.awt.*;
import javax.swing.JApplet;

public class Lab4 extends JApplet
{
  //-------------------------------------------------
  // Use Graphics methods to add content to the drawing canvas
  //-------------------------------------------------
  public void paint(Graphics page)
  {
	page.drawRect(101,10,50,50);
	page.drawOval(101,10,20,20);
	page.drawOval(130,10,20,20);
	page.drawOval(110,45,30,10);
	page.drawRect(116,60,20,25);
	page.drawRect(81,85,90,150);
	page.drawLine(171,95,221,229);
	page.drawLine(171,135,221,235);
	page.drawOval(218,228,20,18);
	page.drawString("He only has one arm but no one will feel bad for him.", 250, 200);
	page.setColor(Color.blue);	
	page.fillOval(218,228,20,18);
	page.setColor(Color.red);
	page.fillRect(81,85,90,150);
	page.setColor(Color.black);
	page.fillRect(116,60,20,25);
	page.setColor(Color.yellow);
	page.fillOval(101,10,20,20);
	page.fillOval(130,10,20,20);
	page.setColor(Color.cyan);
	page.fillOval(110,45,30,10);
  }
}
