//==========================================
//Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Ziyang Zhang
// CMPSC 111 Spring 2017
// Lab 4
// Feb 9 2017

//==========================================

import javax.swing.*;

public class Lab4Display
{
  public static void main(String[] args)
  {
    JFrame window = new JFrame(" Ziyang Zhang ");

    // Add the drawing canvas and do necessary things to
    // make the window appear on the screen!
    window.getContentPane().add(new Lab4());
    window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    window.setVisible(true);
    window.setSize(600, 400);
  }
}

