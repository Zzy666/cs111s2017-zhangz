
//
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Ziyang Zhang
// CMPSC 111 Spring 2017
// Lab 3
// Feb 2 2017
//
// Purpose: calculate tip and bill
//
import java.util.Date; // needed for printing today's date
import java.util.Scanner;
public class lab3
{
	//
	//main method: program execution begins here
	//
	public static void main(String[] args)
	{
	// Label output with name and date:
	System.out.println("Ziyang\nlab #\n" + new Date() + "\n");
	// welcome
	String name;
	Scanner scan = new Scanner(System.in);
	System.out.println("Please enter your name here:");
		 name = scan.next();
	System.out.print("Thank you," + name);
	// pay bill:
	double amount;
	double percent;
	double tip;
	double total;
	int ppl;
	double each;
	System.out.println("please enter your amount of bill:");
		amount = scan.nextInt();
	System.out.println("Please enter the percentage you want to tip:");
		percent = scan.nextInt();
	System.out.println("Your original bill was $" + amount);
		tip = amount * (percent / 100); 
	System.out.println("Your tip amount is $" + tip);
		total = amount + tip;
	System.out.println("Your total bill is $" + total);
	System.out.println("How many people will be splitting the bill?");
		ppl = scan.nextInt();
		each = total / ppl;
	System.out.println("Each person should pay $" + each);
	System.out.println("Have a nice day!");
	System.out.println("Thank you for using our service.");
	}
}

gvim output: //This is not a java program.
Ziyang
lab #
Wed Feb 08 22:46:27 EST 2017

Please enter your name here:
Zhang
Thank you,Zhangplease enter your amount of bill:
40
Please enter the percentage you want to tip:
20
Your original bill was $40.0
Your tip amount is $8.0
Your total bill is $48.0
How many people will be splitting the bill?
3
Each person should pay $16.0
Have a nice day!
Thank you for using our service.

Ziyang
lab #
Wed Feb 08 22:46:05 EST 2017

Please enter your name here:
Ziyang
Thank you,Ziyangplease enter your amount of bill:
70
Please enter the percentage you want to tip:
18
Your original bill was $70.0
Your tip amount is $12.6
Your total bill is $82.6
How many people will be splitting the bill?
8
Each person should pay $10.325
Have a nice day!
Thank you for using our service.

Ziyang
lab #
Wed Feb 08 22:30:50 EST 2017

Please enter your name here:
bjhbjh
Thank you,bjhbjhplease enter your amount of bill:
50
Please enter the percentage you want to tip:
15
Your original bill was $50.0
Your tip amount is $7.5
Your total bill is $57.5
How many people will be splitting the bill?
7
Each person should pay $8.214285714285714
Have a nice day!
Thank you for using our service.

