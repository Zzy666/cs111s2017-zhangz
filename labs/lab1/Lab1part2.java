// Ziyang Zhang
// CMPSC 111
// 19 January 2017
// Lab1
//
// Listing 1.1 from Lewis & Loftus, slightly modified.
// Demonstrates the basic structure of a Java application.

import java.util.Date;

public class Lab1part2
{
    public static void main(String[] args)
    {
	System.out.println("Ziyang" + new Date());
	System.out.println("Euclid tells us:");
	System.out.println("When two lines intersect,");
	System.out.println("the opposite angles are equal to each other.");
    }
}	
