// Ziyang Zhang
// Practical 2, January 27, 2017
// Print rhombus 

import java.util.Date;
public class practical02
{
	public static void main(String[] args)
	{
	  System.out.println("Ziyang Zhang\n" + new Date() + "\n");
	  System.out.println("     /\\      ");
	  System.out.println("    /  \\     ");
	  System.out.println("   /    \\    ");
	  System.out.println("   \\    /    ");
	  System.out.println("    \\  /     ");
	  System.out.println("     \\/      ");
	}
}
