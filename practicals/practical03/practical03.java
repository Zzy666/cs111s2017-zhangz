import java.util.Date; // needed for printing today's date

public class practical02
{
	//main method: program execution begins here
	public static void main(String[] args)
	{
	  // Label output with name and date:
	  System.out.println("Ziyang\npractical03\n" + new Date() + "\n";
	  // Variable:
	  int int2m = 105/100; // interest rate of 2 month deposit
	  int int1y = 110/100; // interest rate of 1 year deposit
	  int money = 10000; // the money to deposit
	  int money1; // the money after 1 year
	  int money2; // the money after 1 year and 2 months
	  // Compute values:
	  money1 = money * int1y + money;
	  money2 = money1 * int2m + money1;
	  System.out.println("The money deposited at first: " + money)
	  System.out.println("The money could be withdrawed");
	  System.out.println("after one year: " + money1);
	  System.out.println("The money could be withdrawed");
	  System.out.println("after one year and two months: " + money1);
	}
}

