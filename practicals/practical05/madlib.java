//
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Ziyang Zhang
// CMPSC 111 Spring 2017
// Practical 5
// Feb 24 2017
//
// Purpose: create a Mad lib game
//
import java.util.Date; // needed for printing today's date
import java.util.Scanner; // needed for scanner

public class madlib
{
	public static void main (String[] args)
	{
	System.out.println("Ziyang\npractical 5\n" + new Date() + "\n");	
	Scanner scan = new Scanner(System.in);
	System.out.println("Enter the name of a person you dislike:");
	String a = scan.next();
	System.out.println("Enter the name of the animals(plural) you are afraid of:");
	String b = scan.next();
	System.out.println("Enter an integer more than 100:");
	String c = scan.next();
	System.out.println("Enter a place you think is horrible:");
	String d = scan.next();
	System.out.println(a + ", on " + new Date() +", is walking to " + d + ", but suddenly meets " + c + " " + b + ". Then they fight, but " + a + " loses. When " + a + " leaves, " + a + " says to " + b + " 'Stay here! My mom is coming.'");
	}
}
