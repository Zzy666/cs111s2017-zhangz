//*****************************
// CMPSC 111
// Practical 6
// April 3, 2017
// Ziyang Zhang
// Purpose: 
//*****************************

import java.util.Date;
import java.util.Scanner;
import java.util.Random;
public class GuessMyNumber
{
public static void main (String[] args)
{
Scanner scan = new Scanner(System.in);
Random rand = new Random();
System.out.println("Ziyang\npractical 6\n" + new Date() + "\n");
System.out.println("Try to guess a number between 1 and 100");
int a, b, numTries;
a = rand.nextInt(100)+1;
b = scan.nextInt();
numTries = 0;
while (a != b)
{
if (b < a)
{
System.out.println("Too low! Try again.");
b = scan.nextInt();
numTries++;
}
else if (b > a)
{
System.out.println("Too High! Try again.");
b = scan.nextInt();
numTries++;
}
}
System.out.println("After" + numTries + "times of guess, you get the correct answer.");
}
}
