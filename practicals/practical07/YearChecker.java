//*****************************
// CMPSC 111
// Practical 8
// October 30, 2015
//
// Purpose: determine if user's year input is a leap year,
// cicada brood II emergence year, or a peak sunspot year.
//*****************************

public class YearChecker
{
    //Create instance variables
    int y;
    //Create a constructor
    public YearChecker(int year)
    {
        year = y;
    }

    // a method that checks if the user's input year is a leap year
    public boolean isLeapYear(int year)
    {
        // TO DO: complete
        boolean a;
		if (((year % 100 == 0) && (year % 400 == 0)))
      {
      a = true;
      return a;
      }
    else if (year % 4 == 0)
    {
    a = true;
    return a;
    }
	}

    // a method that checks if the user's input year is a cicada year
    public boolean isCicadaYear(int year)
    {
        // TO DO: complete
        boolean b;
	if (year == 2013)
  {
  b = true;
  return b;
  }
	else if ((year > 2013) && ((year-2013) % 17 == 0))
    {
    b = true;
    return b;
    }
	else if ((year < 2013) && ((2013-year) % 17 == 0))
    {
    b = true;
    return b;
    }
    }

    // a method that check if the user's input year is a sunspot year
    public boolean isSunspotYear(int year)
    {
        // TO DO: complete
        boolean c;
	if (year == 2013)
  {
  c = true;
  return c;
  }
	else if ((year > 2013) && ((year-2013) % 11 == 0))
  {
  c = true;
  return c;
  }
	else if ((year < 2013) && ((2013-year) % 11 == 0))
  {
  c = true;
  return c;
  }
    }
}
