// This example is derived from a Java source code snippet found at:
// http://pages.cs.wisc.edu/~hasti/cs368/JavaTutorial/NOTES/Exceptions.html

public class ExceptionExample {

  static void throwsExceptions(int k, int[] A, String S) {
    int j = 1 / k;
    int len = A.length + 1;
    char c,d;

    try {
      c = S.charAt(0);
      d = S.charAt(2);
      if (k == 10) j = A[3];
    }
    catch (StringIndexOutOfBoundsException ex) {
      System.out.println("String error");
      System.out.println("The stack trace is:");
      ex.printStackTrace();
    }
    catch (ArrayIndexOutOfBoundsException ex) {
      System.out.println("Array error");
      System.out.println("The stack trace is:");
      ex.printStackTrace();
    }
    catch (ArithmeticException ex) {
      System.out.println("Arithmetic error");
    }

    catch (NullPointerException ex) {
      System.out.println("Null pointer");
      System.out.println("The stack trace is:");
      ex.printStackTrace();
    }
    finally {
      System.out.println("In the finally clause");
    }
    System.out.println("After the try block");
  }

  public static void main(String[] args) {
    int[] X = {0,1,2};
     throwsExceptions(0, X, "hi");
    // throwsExceptions(10, X, "");
    //throwsExceptions(10, X, "bye");
    //throwsExceptions(10, X, null);

  }

}
